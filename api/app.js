var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var mapleRouter = require("./routes/maple");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use("/api/v1/maple", mapleRouter);
app.get("/health", async (req, res, next) => {
  res.sendStatus(200);
});

module.exports = app;
