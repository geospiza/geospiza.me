var express = require("express");
var router = express.Router();

const fetch = require("node-fetch");

let base_url = "https://dreamms.gg";

async function get_user_stats() {
  let resp = await fetch(base_url);
  let html = await resp.text();
  let re = /Online Players.*?(\d+)/;
  let count = parseInt(html.match(re)[1]);
  return {
    timestamp: new Date().toISOString(),
    usercount: count,
  };
}

router.get("/dream", async function (req, res, next) {
  let data = await get_user_stats();
  res.send(data);
});

module.exports = router;
