"""Script to migrate a Nadeko database from one server ID to another.
Example:
    python3 migrate_nadeko.py NadekoBot.db 837129629218897972 937129629218897973
"""
import argparse
import shutil
import sqlite3
from pathlib import Path


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("database_path", type=str, help="Path to the database file")
    parser.add_argument("old_server_id", type=str, help="Old discord server ID")
    parser.add_argument("new_server_id", type=str, help="New discord server ID")
    return parser


def get_guild_id_columns(cursor):
    cursor.execute(
        """
            SELECT
                m.name as table_name,
                p.name as column_name
            FROM
                sqlite_master AS m
            JOIN
                pragma_table_info(m.name) AS p
            WHERE
                p.name like "%uild%"
            ORDER BY
                m.name,
                p.cid
        """
    )
    return cursor.fetchall()


def replace_guild_id(cursor, old_server_id, new_server_id):
    table_columns = get_guild_id_columns(cursor)
    count = 0
    for table_name, column_name in table_columns:
        cursor.execute(
            f"""
                UPDATE {table_name} 
                SET {column_name} = ? 
                WHERE {column_name} = ?
            """,
            (new_server_id, old_server_id),
        )
        count += cursor.rowcount
    return count


def create_backup(db_path):
    db_path = Path(db_path)
    backup_path = db_path.with_suffix(db_path.suffix + ".bak")
    if backup_path.exists():
        raise RuntimeError("Backup already exists")
    shutil.copy(db_path, backup_path)


def main():
    args = create_parser().parse_args()
    print(
        f"Replacing server ID in {args.database_path} "
        f"from {args.old_server_id} to {args.new_server_id}"
    )
    create_backup(args.database_path)
    conn = sqlite3.connect(args.database_path)
    cursor = conn.cursor()
    modified_count = replace_guild_id(cursor, args.old_server_id, args.new_server_id)
    print(f"Replaced {modified_count} rows")
    conn.commit()
    cursor.close()
    print("Done!")


if __name__ == "__main__":
    main()
