import shutil
import sqlite3
from pathlib import Path

import pytest

import migrate_nadeko

DATA_DIR = Path(__file__).parent / "data"
GUILD_ID = 837129629218897972


@pytest.fixture()
def test_path(tmp_path):
    shutil.copytree(DATA_DIR, tmp_path / "data")
    return tmp_path / "data"


@pytest.fixture()
def cursor(test_path):
    conn = sqlite3.connect(test_path / "NadekoBot.db")
    yield conn.cursor()
    conn.close()


def test_create_backup(test_path):
    assert (test_path / "NadekoBot.db").exists()
    migrate_nadeko.create_backup(test_path / "NadekoBot.db")
    assert (test_path / "NadekoBot.db.bak").exists()


def count_guild_id(cursor, guild_id):
    table_columns = migrate_nadeko.get_guild_id_columns(cursor)
    assert len(table_columns) > 0

    count = 0
    for table_name, column_name in table_columns:
        cursor.execute(
            f"""
                SELECT COUNT(*)
                FROM {table_name}
                WHERE {column_name} = ?
            """,
            (guild_id,),
        )
        value = cursor.fetchone()[0]
        if value > 0:
            print(f"{table_name} has guild_id in {column_name} {value} times")
        count += value
    return count


def test_guild_id_exists_in_multiple_tables(cursor):
    count = count_guild_id(cursor, GUILD_ID)
    assert count > 0


def test_guild_id_is_replaced(cursor):
    old_guild_id = GUILD_ID
    new_guild_id = GUILD_ID + 1
    modified_count = migrate_nadeko.replace_guild_id(cursor, old_guild_id, new_guild_id)
    count = count_guild_id(cursor, old_guild_id)
    assert count == 0
    count = count_guild_id(cursor, new_guild_id)
    assert count > 0
    assert modified_count == count
