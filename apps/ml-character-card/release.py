#!/usr/bin/env python3
from subprocess import run
from pathlib import Path

with open("Cargo.toml") as fp:
    for line in fp.readlines():
        if line.startswith("version"):
            version = line.split("=")[1].replace('"', "").strip()

run("cargo build --release".split(), check=True)

path = list(Path("target/release").glob("*.exe"))[0]
name = path.name.split(".exe")[0] + f"-{version}.exe"

run(
    f"gsutil cp {path} gs://geospiza/downloads/{name}".split(),
    check=True,
    shell=True,
)
