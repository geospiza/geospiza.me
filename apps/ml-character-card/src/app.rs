use eframe::{egui, epi};
use epi::http::{Request, Response};
use serde_json;
use std::sync::mpsc::Receiver;

// https://github.com/emilk/egui/blob/master/egui_demo_lib/src/apps/http_app.rs
struct Resource {
    response: Response,
    text: Option<String>,
    image: Option<Image>,
}

impl Resource {
    fn from_response(response: Response) -> Self {
        let content_type = response.content_type().unwrap_or_default();
        let image = if content_type.starts_with("image/") {
            Image::decode(&response.bytes)
        } else {
            None
        };
        let text = response.text();
        Self {
            response,
            text,
            image,
        }
    }
}

#[derive(Default)]
struct TextureManager {
    loaded_url: String,
    texture_id: Option<egui::TextureId>,
}

impl TextureManager {
    fn texture(
        &mut self,
        frame: &mut epi::Frame<'_>,
        url: &str,
        image: &Image,
    ) -> Option<egui::TextureId> {
        if self.loaded_url != url {
            if let Some(texture_id) = self.texture_id.take() {
                frame.tex_allocator().free(texture_id);
            }

            self.texture_id = Some(
                frame
                    .tex_allocator()
                    .alloc_srgba_premultiplied(image.size, &image.pixels),
            );
            self.loaded_url = url.to_owned();
        }
        self.texture_id
    }
}

struct Image {
    size: (usize, usize),
    pixels: Vec<egui::Color32>,
}

impl Image {
    fn decode(bytes: &[u8]) -> Option<Image> {
        use image::GenericImageView;
        let image = image::load_from_memory(bytes).ok()?;
        let image_buffer = image.to_rgba8();
        let size = (image.width() as usize, image.height() as usize);
        let pixels = image_buffer.into_vec();
        assert_eq!(size.0 * size.1 * 4, pixels.len());
        let pixels = pixels
            .chunks(4)
            .map(|p| egui::Color32::from_rgba_unmultiplied(p[0], p[1], p[2], p[3]))
            .collect();

        Some(Image { size, pixels })
    }
}

pub struct MyApp {
    texture_manager: TextureManager,
    avatar_in_progress: Option<Receiver<Result<Response, String>>>,
    avatar_result: Option<Result<Resource, String>>,
    character_in_progress: Option<Receiver<Result<Response, String>>>,
    character_result: Option<Result<Resource, String>>,
    character_name: String,
}

impl Default for MyApp {
    fn default() -> Self {
        Self {
            texture_manager: Default::default(),
            avatar_in_progress: None,
            avatar_result: None,
            character_in_progress: None,
            character_result: None,
            character_name: Default::default(),
        }
    }
}

macro_rules! display_char_row {
    ($ui:expr, $map:expr, $k:expr ) => {
        $ui.label($k);
        let val = &$map[$k];
        $ui.label(if val.is_string() {
            format!("{}", val.as_str().unwrap())
        } else {
            format!("{}", val)
        });
        $ui.end_row();
    };
}

impl epi::App for MyApp {
    fn name(&self) -> &str {
        "Character Card"
    }

    fn update(&mut self, ctx: &egui::CtxRef, frame: &mut epi::Frame<'_>) {
        // duplication, not sure what an better way of doing this is
        if let Some(receiver) = &mut self.avatar_in_progress {
            if let Ok(response) = receiver.try_recv() {
                self.avatar_in_progress = None;
                self.avatar_result = Some(response.map(Resource::from_response));
            }
        }
        if let Some(receiver) = &mut self.character_in_progress {
            if let Ok(response) = receiver.try_recv() {
                self.character_in_progress = None;
                self.character_result = Some(response.map(Resource::from_response));
            }
        }
        egui::TopBottomPanel::bottom("info").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.hyperlink_to("made by geospiza", "https://geospiza.me");
                ui.label("|");
                ui.hyperlink_to(
                    "source code",
                    "https://bitbucket.org/geospiza/geospiza.me/src/main/apps/ml-character-card",
                );
                ui.label("|");
                ui.label(format!("version {}", env!("CARGO_PKG_VERSION")));
            })
        });

        egui::TopBottomPanel::top("search").show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                if let Some((avatar, character)) = ui_character_name(ui, &mut self.character_name) {
                    {
                        let repaint_signal = frame.repaint_signal();
                        let (sender, receiver) = std::sync::mpsc::channel();
                        self.avatar_in_progress = Some(receiver);
                        frame.http_fetch(avatar, move |response| {
                            sender.send(response).ok();
                            repaint_signal.request_repaint();
                        });
                    }
                    {
                        let repaint_signal = frame.repaint_signal();
                        let (sender, receiver) = std::sync::mpsc::channel();
                        self.character_in_progress = Some(receiver);
                        frame.http_fetch(character, move |response| {
                            sender.send(response).ok();
                            repaint_signal.request_repaint();
                        });
                    }
                }
            })
        });

        if self.character_name.len() > 0
            && self.avatar_in_progress.is_none()
            && self.character_in_progress.is_none()
            && self.avatar_result.is_some()
            && self.character_result.is_some()
        {
            egui::Window::new(&self.character_name)
                .default_pos(egui::Pos2::new(100.0, 40.0))
                .collapsible(false)
                .show(ctx, |ui| {
                    ui.horizontal(|ui| {
                        if let Some(result) = &self.character_result {
                            match result {
                                Ok(resource) => {
                                    if let Some(text) = &resource.text {
                                        let ch: serde_json::Value =
                                            serde_json::from_str(text).unwrap();
                                        egui::Grid::new("info").num_columns(2).striped(true).show(
                                            ui,
                                            |ui| {
                                                display_char_row!(ui, ch, "name");
                                                display_char_row!(ui, ch, "guild");
                                                display_char_row!(ui, ch, "level");
                                                display_char_row!(ui, ch, "job");
                                                display_char_row!(ui, ch, "exp");
                                                display_char_row!(ui, ch, "quests");
                                                display_char_row!(ui, ch, "cards");
                                                display_char_row!(ui, ch, "donor");
                                                display_char_row!(ui, ch, "fame");
                                            },
                                        );
                                    }
                                }
                                Err(error) => {
                                    ui.add(
                                        egui::Label::new(if error.is_empty() {
                                            "Error"
                                        } else {
                                            error
                                        })
                                        .text_color(egui::Color32::RED),
                                    );
                                }
                            }
                        }
                        ui.add_space(50.0);
                        if let Some(result) = &self.avatar_result {
                            match result {
                                Ok(resource) => {
                                    if let Some(image) = &resource.image {
                                        if let Some(texture_id) = self.texture_manager.texture(
                                            frame,
                                            &resource.response.url,
                                            &image,
                                        ) {
                                            let size = egui::Vec2::new(
                                                image.size.0 as f32,
                                                image.size.1 as f32,
                                            );
                                            ui.image(texture_id, size);
                                        }
                                    }
                                }
                                Err(error) => {
                                    ui.add(
                                        egui::Label::new(if error.is_empty() {
                                            "Error"
                                        } else {
                                            error
                                        })
                                        .text_color(egui::Color32::RED),
                                    );
                                }
                            }
                        }
                    });
                });
        }

        // The central panel is required, otherwise the windows freak out
        egui::CentralPanel::default().show(ctx, |_ui| {});

        // Resize the native window to be just the size we need it to be:
        frame.set_window_size(ctx.used_size());
    }
}

fn ui_character_name(ui: &mut egui::Ui, character_name: &mut String) -> Option<(Request, Request)> {
    let mut trigger_fetch = false;

    ui.horizontal(|ui| {
        ui.label("character name:");
        trigger_fetch |= ui.text_edit_singleline(character_name).lost_focus();
        trigger_fetch |= ui.button("▶").clicked();
    });

    let base = "https://us-west2-geospiza.cloudfunctions.net/maplelegends-api-proxy";
    if trigger_fetch {
        Some((
            Request::get(format!("{}/getavatar?name={}", base, character_name)),
            Request::get(format!("{}/character?name={}", base, character_name)),
        ))
    } else {
        None
    }
}
