#![windows_subsystem = "windows"]
#![forbid(unsafe_code)]
#![cfg_attr(not(debug_assertions), deny(warnings))] // Forbid warnings in release builds
#![warn(clippy::all, rust_2018_idioms)]

use eframe::egui;

// When compiling natively:
#[cfg(not(target_arch = "wasm32"))]
fn main() {
    let app = ml_character_card::MyApp::default();
    let native_options = eframe::NativeOptions {
        initial_window_size: Some(egui::Vec2::new(400.0, 300.0)),
        ..eframe::NativeOptions::default()
    };
    eframe::run_native(Box::new(app), native_options);
}
