import requests
from datetime import datetime


def character(request):
    """Proxy to the maplelegends character API."""
    name = request.args.get("name")
    url = f"https://maplelegends.com/api/character?name={name}"
    resp = requests.get(url)
    # also insert the current timestamp of the request
    data = resp.json()

    return (
        dict(timestamp=datetime.utcnow().isoformat(), **data),
        resp.status_code,
        {"ContentType": "application/json", "Access-Control-Allow-Origin": "*"},
    )


if __name__ == "__main__":

    class TestRequest:
        def __init__(self, name):
            self.args = dict(name=name)

    print(character(TestRequest("geospiza")))
