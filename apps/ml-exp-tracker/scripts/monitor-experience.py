#!/usr/bin/env python3
# usage: python3 monitor-experience.py <NAME>
import json
import time
from argparse import ArgumentParser
from datetime import datetime
from urllib.request import urlopen

parser = ArgumentParser()
parser.add_argument("name", type=str, help="name of the user to track experience")
parser.add_argument(
    "--period",
    type=int,
    default=30,
    help="number of seconds to wait before the next request",
)
parser.add_argument("--output", type=str, default="character.ndjson")

args = parser.parse_args()
url = f"https://maplelegends.com/api/character?name={args.name}"


def print_data(data):
    r = data
    fmt = f'{r["timestamp"][:19]} {data["name"]} level {r["level"]} {r["exp"]}'
    print(fmt)


while True:
    resp = urlopen(url)
    data = dict(dict(timestamp=datetime.now().isoformat()), **json.load(resp))
    print_data(data)
    with open(args.output, "a") as fp:
        fp.write(json.dumps(data)+"\n")
    time.sleep(args.period)
