locals {
  name    = "character"
  tmp_zip = "/tmp/${local.name}.zip"
}

data "archive_file" "archive" {
  type        = "zip"
  source_dir  = "${path.module}/../functions/${local.name}"
  output_path = local.tmp_zip
}

resource "google_storage_bucket_object" "archive" {
  name   = "functions/${local.name}-${data.archive_file.archive.output_sha}.zip"
  bucket = var.bucket
  source = local.tmp_zip
}

// proxy cloud function for getting character information
resource "google_cloudfunctions_function" "default" {
  name                  = local.name
  entry_point           = local.name
  runtime               = "python38"
  available_memory_mb   = 128
  source_archive_bucket = var.bucket
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  labels = {
    "deployment-tool" = "cli-gcloud"
  }
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.default.project
  region         = google_cloudfunctions_function.default.region
  cloud_function = google_cloudfunctions_function.default.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
