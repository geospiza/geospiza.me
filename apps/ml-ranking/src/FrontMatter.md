<script>
  import Editor from "./components/Editor.svelte";
</script>

# Level 200 characters in MapleLegends

This is a data visualization of level 200 characters in
[MapleLegends](https://maplelegends.com/), an old-school
[MapleStory](https://en.wikipedia.org/wiki/MapleStory) private server started in 2015. This dataset was scraped from the [ranking
page](https://maplelegends.com/ranking/all). Only characters that have made it
to level 200, the max level in the game, are included in the `ranking` table.
The `leveling` table includes the [leveling
history](https://maplelegends.com/levels?name=geospiza) of each of the
characters in the ranking.

Find the [source code
here](https://bitbucket.org/geospiza/geospiza.me/src/main/apps/ml-ranking/). See
[this notebook](https://geospiza.me/2020-12-29-ml-200/) for exploratory analysis
on the data. The compiled SQLite database can be directly queried within the
browser via [SQL.js](https://sql.js.org/#/) in the editor tab. Visualizations
are rendered from the database using [Plotly](https://plotly.com/javascript/).

Here are direct links to the JSON and SQLite datasets, last updated on 2021-01-01.

{#each ['ranking.json', 'leveling.json', 'ranking.db'] as file}

- [{file}]({`https://storage.googleapis.com/geospiza/scrapers/ranking/data/${file}`})

{/each}

[Take me to the homepage.](https://geospiza.me)
