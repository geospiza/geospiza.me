## Design Notes

I learned quite a bit putting together this site, so here are some notes on the
process and challenges.

### Data collection and exploration

The ranking page has some useful information about players. When you are logged
in, you can get a history of all of your levels which includes the timestamp and
location where you leveled. When you're looking at any other character, you can
only see the leveling log. My initial idea was to compile this data for the top
50 characters or so from each class, but I realized there were some issues with
having abbreviated histories and different level ranges. Instead, I opted to
only collect data from level 200 characters. These characters all had the same
thing in common -- they reached the level cap. The set of characters is also
small enough for analysis in the browser, and I ended up building a few neat
plots.

There are a couple of things about this dataset that should be kept in mind.
First, not all of the leveling history is included on the characters. It looks
like leveling logs started a few months after the inception of the server.
Secondly, the set of characters may not be up to date because I have to manually
run a scraper to collect the data. The scraper source can be found
[here](https://bitbucket.org/geospiza/geospiza.me/src/main/scrapers/ranking/).

From my [exploratory notebook](https://geospiza.me/2020-12-29-ml-200/), I made
an interesting time to level plot using a grouped box plot. From this, I started
down my journey on build this visualization.

<img src="./time-to-level.png"
     alt="time-to-level"
     style="background: gray; max-width:100%" />

### Application design

#### Using bindings in svelte

I've written a few applications using svelte, [OwlRepo](https://owlrepo.com/)
being the largest of them all so far. In this project through, I finally
understood how [bindings](https://svelte.dev/tutorial/bind-this) worked. This is
the way that you can pass data from child components to parent components, as
well as binding pointers to dom elements for javascript libraries.

I used bindings for passing results from the editor component back to each of
the individualized plots. For example, the `Plot` component uses the `Editor`
component for executing queries:

```javascript
<Editor {db} {query} editable={false} visible={isOpen} bind:results />
```

The results variable is bound to the child component, so it can be used by the
`Plot` component with `Plotly`. To get the div for `Plotly`, I bind `this` to
the dom element:

```javascript
<div bind:this={plot} />
```

Then the `plot` variable is used for new object:

```javascript
Plotly.newPlot(plot, ...)
```

#### Using SQLite as a data store

While doing the exploratory analysis in Python, one of the things that I missed
was the ability to query data in SQL. I had read about using SQLite directly in
the browser using wasm, so this is the project where I tried it out. It's
actually pretty incredible that it works so well, and it's much nicer than
manipulating the rows directly in javascript. It also allows for some cool
functionality like writing your own queries directly in an editor, which I made
use of through development.

One of the coolest features is that you can register functions with the SQLite
query engine using javascript functions. There currently isn't support for
aggregate functions, but it's useful nevertheless. For example, here's a
function to get the difference between two dates, since there is no native
timestamp/datetime type in SQLite:

```javascript
db.create_function("date_diff_js", (ts1, ts2, unit) => {
    let parse = (ts) => DateTime.fromISO(ts.replace(" ", "T")).toUTC();
    return parse(ts1).diff(parse(ts2)).as(unit);
}
```

In a query:

```sql
SELECT date_diff_js("2017-03-16 04:00:25.000000", "2017-03-15 06:31:25.000000", "minutes")
-- Returns: 1289
```

#### Using `sql.js` in a web worker

The "time to level" query is the most intensive query of all of the queries on
the dashboard, since it requires pulling all 40k rows into memory. The sqlite
function runs on the main thread, which causes the UI to become unresponsive.
So, I looked into how to get the process to run off the main thread in a web
worker.

I tried using
[surma/rollup-plugin-off-main-thread](https://github.com/surma/rollup-plugin-off-main-thread),
which is a rollup plugin that bundles modules into a file that can be sent off
to a web worker. It turns out that this doesn't work in the case where the
assets are being [written into a public
folder](https://github.com/surma/rollup-plugin-off-main-thread/issues/4), which
is the structure that I'm used to for building these single page web
applications. I think it's unfortunate that the PR for fixing this hasn't been
merged in over a year, so I decided to look into some other options.

[darionco/rollup-plugin-web-worker-loader](https://github.com/darionco/rollup-plugin-web-worker-loader#readme)
worked well when I put it in the project. The interface is different that
`off-main-thread`, but I can overlook the differences when it actually works as
advertised. I wasted quite a bit of time trying to get some sort of threading
working, so I was relieved that this worked. The `off-main-thread` author
recommended the use of [comlink](https://github.com/GoogleChromeLabs/comlink)
for abstracting away the message passing involved with web worker code, and
after using it I can recommend it for being a nice, simple abstraction.

The only downside of web workers is that the SQLite queries are no longer
instantaneous when they are small due to the overhead with loading the
web-worker. A small loading animation (or text) helps hide this, but it's still
a bit annoying that there is some lag to load up the wasm module.

#### Using `codemirror` for editing

I started with just a textarea element for showing the queries I wrote for the
plots. This sucked because tab is not binded to the text editor, but rather to
the web page element. I pulled in codemirror as a dependency since it is a full
featured editor that runs in the browser. I tried out [CodeMirror
6](https://codemirror.net/6/), since it seemed to fit with the style of
programming that I'm doing. I'd say that it's not really fit for general purpose
use yet, but it was enjoyable to use and figure out. The documentation and
examples are lacking though, so I would argue against it's use.

#### Dark mode

To be honest, I'm not sure that using a dark theme is any more aesthetically
pleasing than using the default styling on the page in a light mode. It did
force me to play with settings that I hadn't used before.

As mentioned in the previous section, I downgraded from CodeMirror 6 to
CodeMirror 5 in order to take advantage of the [existing themes that come
bundled](https://codemirror.net/demo/theme.html). There is a
[@codemirror/theme-one-dark](https://github.com/codemirror/theme-one-dark)
extension that I could use with the older editor, but it did not highlight the
syntax tags from the
[@codemirror/lang-sql](https://github.com/codemirror/lang-sql) extension. Plotly
has some simple options for setting the background color and fonts, so it
actually looks pretty nice on a darker background with the default plotting
colors.

### Future Work

There were a few ideas that I didn't come around to implementing. They would be
interesting to add to this application, but overall not worth the time for the
amount of time that I would use it for.

#### Annotating a single player's history on the "time to level" plot

An interesting idea to make the time to level plot more interactive would be to
add a search bar where you could look at a single player's leveling history and
overlay the points on the box plots (probably as orange points). It would be
cool to be able to compare your data points to the average and see how you stack
with others. I'm not sure if this is actually possible to add with Plotly's [box
plot api](https://plotly.com/javascript/box-plots/). For searching through the
different values, a fuzzy search box using [Fuse.js](https://fusejs.io/) and a
parameterized query into the leveling history would probably work well.

#### Adding plotting functionality to the editor

There is a completely functional SQL editor in the editor tab that is also being
used to query the data for the plots in the dashboard. It would be
straightforward to add support plotting data for custom queries. For example,
imagine a query for determining which class has the most fame:

```sql
SELECT job, sum(fame) FROM ranking
```

This would be best suited as a bar chart. There are a couple of transforms that
I defined in a utility function that can be mapped to each chart type:

- `transformResults1D`
- `transformResults2D`
- `transformResultsGrouped1D`

These take the rows from SQL query and transform them into traces that Plotly
can use for plotting. Each one of these can be used for a different type of
chart i.e. `histogram`, `line`, `box` respectively. A dropdown menu would be the
best way to map into these transforms.

#### Sharing queries using a base64-encoded query parameter

With a functional editor that can be parameterized by a query and a plot type,
it should be possible to share queries with other people by saving the query as
a parameter string that encodes the settings in a JSON blob. It might look
something like this:

```json
{
  "query": "SELECT job, sum(fame) FROM ranking",
  "plot": "bar"
}
```

The results of this string would then be encoded into a URI as follows:

```
editor/ewogICAgInF1ZXJ5IjogIlNFTEVDVCBqb2IsIHN1bShmYW1lKSBGUk9NIHJhbmtpbmciLAogICAgInBsb3QiOiAiYmFyIgp9
```

In order to support the parameter string, the application would have to add in
some routing like [page.js](https://visionmedia.github.io/page.js/) in order to
directly open the page on the editor.
