import App from "./App.svelte";
import "../public/bootstrap.darkly.min.css";

const app = new App({
  target: document.body,
  props: {},
});

export default app;
