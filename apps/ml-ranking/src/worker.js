import { expose } from "comlink";
import initSqlJs from "sql.js";
import { DateTime } from "luxon";

async function fetchData(baseURI = "") {
  let resp = await fetch(`${baseURI}/ranking.db`);
  return new Uint8Array(await resp.arrayBuffer());
}

class DB {
  async init(baseURI) {
    let SQL = await initSqlJs({
      locateFile: (file) => `${baseURI}/vendor/${file}`,
    });
    this._db = new SQL.Database(await fetchData(baseURI));
  }

  async exec(query) {
    this._db.create_function("month_js", (ts) => {
      // these dates are not actually well formed iso dates
      let month = DateTime.fromISO(ts.replace(" ", "T"))
        .startOf("month")
        .toUTC()
        .toISO();
      return month;
    });
    this._db.create_function("date_diff_js", (ts1, ts2, unit) => {
      let parse = (ts) => DateTime.fromISO(ts.replace(" ", "T")).toUTC();
      return parse(ts1).diff(parse(ts2)).as(unit);
    });
    return this._db.exec(query);
  }
}

expose(DB);
