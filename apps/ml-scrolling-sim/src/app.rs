use eframe::{egui, epi};
use include_dir::{Dir, DirEntry};
use rodio::{Decoder, OutputStream, Sink};
use std::io::Cursor;

static DATA_DIR: Dir<'_> = include_dir!("./assets");

#[derive(Default)]
struct TextureManager {
    loaded_url: String,
    texture_id: Option<egui::TextureId>,
}

impl TextureManager {
    fn texture(
        &mut self,
        frame: &mut epi::Frame<'_>,
        url: &str,
        image: &Image,
    ) -> Option<egui::TextureId> {
        if self.loaded_url != url {
            if let Some(texture_id) = self.texture_id.take() {
                frame.tex_allocator().free(texture_id);
            }

            self.texture_id = Some(
                frame
                    .tex_allocator()
                    .alloc_srgba_premultiplied(image.size, &image.pixels),
            );
            self.loaded_url = url.to_owned();
        }
        self.texture_id
    }
}

struct Image {
    size: (usize, usize),
    pixels: Vec<egui::Color32>,
}

impl Image {
    fn decode(bytes: &[u8]) -> Option<Image> {
        use image::GenericImageView;
        let image = image::load_from_memory(bytes).ok()?;
        let image_buffer = image.to_rgba8();
        let size = (image.width() as usize, image.height() as usize);
        let pixels = image_buffer.into_vec();
        assert_eq!(size.0 * size.1 * 4, pixels.len());
        let pixels = pixels
            .chunks(4)
            .map(|p| egui::Color32::from_rgba_unmultiplied(p[0], p[1], p[2], p[3]))
            .collect();

        Some(Image { size, pixels })
    }
}

struct TaggedImage<'a> {
    uri: &'a str,
    image: Image,
}

enum ScrollStatus {
    Success,
    Failure,
}

struct AudioManager {
    _stream: Option<OutputStream>,
    sink: Option<Sink>,
}

impl Default for AudioManager {
    fn default() -> Self {
        Self {
            _stream: None,
            sink: None,
        }
    }
}

impl AudioManager {
    fn new(scroll_status: &ScrollStatus) -> Self {
        let path = DATA_DIR
            .get_file(match scroll_status {
                ScrollStatus::Success => "EnchantSuccess.mp3",
                ScrollStatus::Failure => "EnchantFailure.mp3",
            })
            .unwrap();
        let source = Decoder::new(Cursor::new(path.contents())).unwrap();

        let (stream, stream_handle) = OutputStream::try_default().unwrap();
        let sink = Sink::try_new(&stream_handle).unwrap();
        sink.append(source);
        Self {
            _stream: Some(stream),
            sink: Some(sink),
        }
    }
    fn is_empty(&self) -> bool {
        self.sink.is_none() || self.sink.as_ref().unwrap().empty()
    }
}

pub struct MyApp<'a> {
    texture_manager: TextureManager,
    audio_manager: AudioManager,
    success: Vec<TaggedImage<'a>>,
    failure: Vec<TaggedImage<'a>>,
    animated: bool,
    scroll_status: ScrollStatus,
    frame_number: usize,
    scroll_type: u8,
}

fn parse_uri_prefix(uri: &str) -> u32 {
    uri.split("_").next().unwrap().parse().unwrap()
}

fn load_images<'a>(glob: &str) -> Vec<TaggedImage<'a>> {
    let mut images = Vec::new();
    for entry in DATA_DIR.find(glob).unwrap() {
        match entry {
            DirEntry::File(file) => images.push(TaggedImage {
                uri: file.path().to_str().unwrap(),
                image: Image::decode(file.contents()).unwrap(),
            }),
            _ => (),
        }
    }
    images.sort_by(|a, b| parse_uri_prefix(a.uri).cmp(&parse_uri_prefix(b.uri)));
    return images;
}

impl<'a> Default for MyApp<'a> {
    fn default() -> Self {
        Self {
            texture_manager: Default::default(),
            audio_manager: AudioManager::default(),
            success: load_images("*_success.png"),
            failure: load_images("*_Failure.png"),
            animated: false,
            scroll_status: ScrollStatus::Failure,
            frame_number: 0,
            scroll_type: 60,
        }
    }
}

impl<'a> epi::App for MyApp<'a> {
    fn name(&self) -> &str {
        "Scrolling Simulator"
    }

    fn update(&mut self, ctx: &egui::CtxRef, frame: &mut epi::Frame<'_>) {
        let Self {
            texture_manager,
            audio_manager,
            success,
            failure,
            animated,
            scroll_status,
            frame_number,
            scroll_type,
        } = self;

        egui::TopBottomPanel::bottom("info").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.hyperlink_to("made by geospiza", "https://geospiza.me");
                ui.label("|");
                ui.hyperlink_to(
                    "source code",
                    "https://bitbucket.org/geospiza/geospiza.me/src/main/apps/ml-scrolling-sim",
                );
                ui.label("|");
                ui.label(format!("version {}", env!("CARGO_PKG_VERSION")));
            })
        });

        egui::SidePanel::left("scroll_type").show(ctx, |ui| {
            ui.label("Options");
            ui.selectable_value(scroll_type, 10, "10%");
            ui.selectable_value(scroll_type, 30, "30%");
            ui.selectable_value(scroll_type, 60, "60%");
            ui.selectable_value(scroll_type, 70, "70%");
            ui.selectable_value(scroll_type, 100, "100%");
        });

        egui::TopBottomPanel::bottom("scroll").show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                if audio_manager.is_empty() {
                    if ui.button("scroll item").clicked() {
                        *animated = true;
                        let threshold = (*scroll_type as f32) / 100.0;
                        if rand::random::<f32>() <= threshold {
                            *scroll_status = ScrollStatus::Success
                        } else {
                            *scroll_status = ScrollStatus::Failure
                        }
                    }
                }
            })
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                ui.heading("Scrolling Simulator");
                if *animated {
                    let frames = match *scroll_status {
                        ScrollStatus::Success => success,
                        ScrollStatus::Failure => failure,
                    };
                    // start the audio for the animation
                    if *frame_number == 0 {
                        *audio_manager = AudioManager::new(scroll_status);
                    }
                    let divisor = 8;
                    let TaggedImage { uri, image } = &frames[*frame_number / divisor];
                    if let Some(texture_id) = texture_manager.texture(frame, &uri, &image) {
                        let size = egui::Vec2::new(image.size.0 as f32, image.size.1 as f32);
                        ui.image(texture_id, size);
                    }
                    ui.ctx().request_repaint();
                    *frame_number += 1;
                    if *frame_number >= frames.len() * divisor {
                        *animated = false;
                        *frame_number = 0;
                    }
                }
            })
        });

        // Resize the native window to be just the size we need it to be:
        frame.set_window_size(ctx.used_size());
    }
}
