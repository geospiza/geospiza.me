#!/usr/bin/env python3
from subprocess import run
from pathlib import Path

with open("Cargo.toml") as fp:
    for line in fp.readlines():
        if line.startswith("name"):
            name = line.split("=")[1].replace('"', "").replace("-", "_").strip()
            break

run("cargo build --release --lib --target wasm32-unknown-unknown".split(), check=True)
run(
    (
        f"wasm-bindgen target/wasm32-unknown-unknown/release/{name}.wasm "
        "--out-dir site --no-modules --no-typescript"
    ).split(),
    check=True,
)
