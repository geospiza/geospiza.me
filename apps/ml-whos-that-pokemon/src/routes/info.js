export async function get({ query }) {
  let id = parseInt(query.get("id"));
  let resp = await fetch(`https://maplelegends.com/lib/monster?search=${id}&json=1`);
  let data = await resp.json();
  return {
    headers: {
      "content-type": "application/json"
    },
    body: data[0]
  };
}
