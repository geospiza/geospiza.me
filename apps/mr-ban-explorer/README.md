# ban appeal explorer

## Get started

Install the dependencies...

```bash
cd svelte-app
npm install
```

...then start [Rollup](https://rollupjs.org):

```bash
npm run dev
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app
running. Edit a component file in `src`, save it, and reload the page to see
your changes.

## Building and running in production mode

To create an optimised version of the app:

```bash
npm run build
```


## Notes

https://mapleroyals.com/forum/threads/mapleroyals-game-terms-conditions.86769/

Major Infractions:

* Account Hacking 
* Game Hacking 
* Server Attack 
* Malicious Programs
* Illegal Content 
* Game Monitoring
* Real World Trading
* Improper Files
* Account Sharing

Moderate Infractions:

* Staff Impersonation
* Solicitation
* Information Harvesting (Stalking)
* Glitch Abuse 
* Robotic Play
* Vote Abuse

Minor Infractions:

* Objectionable Behavior (Harassment)
* Objectionable Behavior (Disruption)
* Hate Speech
* Inappropriate Content
* Kill-stealing
* Map Looting

Infractions by Extension:

* Believable Claim
* Ban Evasion
* Associative Rule-breaking
