# Ban appeals in MapleRoyals

_Last updated 2021-01-10_

This is a web application for viewing and categorizing [ban appeal
posts](https://mapleroyals.com/forum/forums/ban-appeal.8/) in
[MapleRoyals](https://mapleroyals.com/). These are scraped forum posts for
players appealing bans for breaking the [MapleRoyals terms and
conditions](https://mapleroyals.com/forum/threads/mapleroyals-game-terms-conditions.86769).
See the notes for more details.

Click on a row to see the posts. Use the left and right arrow keys to navigate
between posts. Posts are shown in random order.

[Take me to the homepage.](https://geospiza.me)
