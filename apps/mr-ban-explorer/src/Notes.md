<script>
export let db;
import Plot from "./components/Plot.svelte"
import {transformResults2D} from "./utils.js"
</script>

## Notes

The idea for this site was to support exploration of the various forum posts in
the MapleRoyals forums. I find it fascinating that the staff there decided to
make ban appeals a public matter. I saw a Reddit post that brought up some drama
from their ban appeals section, so I decided to start a little scraping project.
It would be neat to break down posts by categories to get a sense of what types
of appeals were being made. The idea was this: scrape the posts, manually
classify a few posts, train a model to label the rest, and then make some
visualizations on those labels. It didn't get as far as I envisioned it because
labeling data was far more challenging that I initially anticipated.

### Scraping data

Scraping the forums was a bit tricky. I did it in two parts. First I scraped the
list of all the forum posts to get a set of links to scrape. Then I scraped the
individual posts themselves. I made a tiny mistake when scraping the post text.
In xpath, the domain-specific language for scraping html pages, I ran the
following over each post in the thread:

```
.//blockquote[contains(@class, 'messageText')]/text()
```

This only looks at the text at the top-level of the `messageText` class, and I
found out after scraping all 14k posts that I was missing bold, italics, and
link text. I never got around to a second round of scraping, so the dataset is
currently missing text (which accounts for about 4k post).

I started trying to post process the data using Pandas, but it got kind of
tricky dealing with the nested data structure that naturally represents the
threads. I pulled out PySpark and wrote a script to explode the data and stick
it into a SQLite database that could be used inside of a browser for analysis.
The 14k posts ends up being around 23mb in size, which I am hosting from a cloud
bucket.

I've used this in another project, where I'm reusing a bunch of the same
elements here. I did some basic exploratory analysis on the data and found some
trends that I haven't dedicated much time to.

<Plot {db} query={`select month_js(start_date) as month, count(distinct thread_id) as n from threads where start_date is not null group by 1`} transform={transformResults2D} layout={{title: "Number of appeals per month"}}/>

Note the dip in the number of posts in early 2017, which I believe is the time
when Royals was doing its source code upgrade that broke a lot of things. There
is also a very notable covid-19 peak in April from all the folks coming into the
private Maplestory scene.

### An explorer for manual labeling

After building the database, I wrote this web-app. Querying SQLite directly for
the data feels intuitive to me. The explorer page lists everything on the
threads. When you click on an item, it brings up the single thread and all the
posts associated with it. Here, I added a few things for classification. For
each post, I add an entry in IndexedDB using `localforge` with the type of
offense the the ban appeal was for, along with its status of being banned or
unbanned. I chose to randomize the posts on the explorer to make it easier to go
through more examples. There's also a categorized page that only includes items
that have been categorized by hand.

I think it's awesome that I can insert into the SQLite database and join against
the data in local storage. It feels very elegant.

After labeling 177 posts by hand, I realized that this whole process of labeling
data is tedious. Most of the posts end up being either auto-bans, hackers
request amnesty, or self-bans. There are almost 20 different labels, so there is
a strong class imbalance here. Most classification models do best when there are
enough examples from all of the classes.

### Plan for automatic labeling

My original idea for creating the labels was to train a [Naive Bayes
classifier](https://en.wikipedia.org/wiki/Naive_Bayes_classifier) on the text.
In order to do this, I need ground truth data. The manual classification hadn't
gone all that well for a variety of reasons. First, the thread can be somewhat
ambiguous about the actual reason of the ban. Some of the posts were missing
text because of a bug I made during scraping. Finally, the class imbalance made
it tedious to actually collect data.

I tried out [Snorkel](https://www.snorkel.org/) for programmatically generating
training data. I like this idea a lot since I just need to create labeling
functions to generate noisy labels for the data. In practice, the large number
of labels does not help. I would have to reconsider the labels that I use. For
example, I may try clustering the data using LSA or LDA first, figure out what
the topics are, and then label the data with the ban/unbanned status.

### Ethical concerns

At first, I was a bit concerned about whether scraping this data was okay. After
reading through a number of them on the public forum and seeing the amount of
hostility from hackers and the good will from staff members, I've come to terms
with building this database. The data is available by policy of public ban
appeals. I think this data can be used for good -- to highlight the efficiencies
and inefficiencies of this model of discipline, as well as the behavior of
players and staff in a public forum. That being said, I am willing to take the
data offline by request.

### Future work

Here are some ideas that I didn't come around to implementing, mostly because of
a lack of interest.

#### Dashboard from scraped data

Even though the data is not categorized based on the type of abuse, there are
still some interesting things that can be inferred from the raw data itself.
First, the posts that are the most popular could be pushed to the surface. We
can also determine who the staff members are (since they may have retired at the
time of the scrape). There are some things that could be done with the raw
vectorized data, such as word clouds and such.

#### Banned/Unbanned

Instead of trying to categorize data into 20-something categories, I could try
looking at posts that are false bans. This number alone would be interesting to
look to see what percentage of appeals are approved. I would like to have some
categorization of posts. I think a list including hacking, real world trading,
and self bans would be sufficient.
