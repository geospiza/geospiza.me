import localforage from "localforage";

async function storeThread(thread_id, data) {
  // this might be inefficient since all the data is being stored in a single object
  let key = "categorized";
  let obj = (await localforage.getItem(key)) || {};
  obj[thread_id] = data;
  await localforage.setItem(key, obj);
}

async function loadCategorized() {
  let key = "categorized";
  return (await localforage.getItem(key)) || {};
}

async function loadThread(thread_id) {
  let obj = await loadCategorized();
  let res = obj[thread_id];
  return res;
}

function formatRow(key, value) {
  return `(
        ${key},
        '${value.timestamp}',
        '${value.category}',
        '${value.reason}',
        '${value.status}'
      )`;
}
async function insertCategorizedData(db) {
  await db.exec(`
    CREATE TABLE IF NOT EXISTS categorized (
      thread_id INTEGER PRIMARY KEY,
      timestamp TEXT,
      category TEXT,
      reason TEXT,
      status TEXT
    );
  `);
  let values = Object.entries(await loadCategorized())
    .map(([key, value]) => formatRow(key, value))
    .join(",");
  await db.exec(`
    REPLACE INTO categorized (
      thread_id, timestamp, category, reason, status
    )
    VALUES ${values}
  `);
}

async function onChangeStored(db, thread_id) {
  let value = await loadThread(thread_id);
  await db.exec(`
    REPLACE INTO categorized (
      thread_id, timestamp, category, reason, status
    )
    VALUES ${formatRow(thread_id, value)}
  `);
  result = (await db.exec(query_threads))[0];
}

export {
  storeThread,
  loadThread,
  loadCategorized,
  insertCategorizedData,
  onChangeStored,
};
