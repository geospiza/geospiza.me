import { zip, groupBy } from "lodash";
import { wrap } from "comlink";
import Worker from "web-worker:./worker.js";

async function initDb() {
  let DB = wrap(new Worker());
  let db = await new DB();
  await db.init("appeals.db", document.baseURI.replace("index.html", ""));
  return db;
}

function transformResultObject(result) {
  return result.values.map((row) =>
    Object.fromEntries(zip(result.columns, row))
  );
}

/// Transform results from sql-js into a trace for plotly.
/// Results are converted from rows into columns of arrays.
function transformResults1D(results, args) {
  let zipped = zip(...results[0].values);
  return [
    {
      x: zipped[0],
      ...args,
    },
  ];
}

function transformResults2D(results, args) {
  let zipped = zip(...results[0].values);
  return [
    {
      x: zipped[0],
      y: zipped[1],
      ...args,
    },
  ];
}

/// Transform results from sql-js into traces for plotly.
/// Values are grouped by the first column in the result.
function transformResultsGrouped1D(results, args) {
  let grouped = groupBy(results[0].values, (row) => row[0]);
  return Object.keys(grouped).map((idx) => {
    let zipped = zip(...grouped[idx]);
    return {
      y: zipped[1],
      name: idx,
      ...args,
    };
  });
}

export {
  initDb,
  transformResultObject,
  transformResults1D,
  transformResults2D,
  transformResultsGrouped1D,
};
