from pathlib import Path
import pandas as pd
from datetime import datetime


def parse_raw(lines):
    data = []
    date = None
    row = {}
    for line in lines:
        # NOTE: stupid hack, startswith isn't working properly
        if any([f"/{year}" in line for year in [2019, 2020, 2021]]):
            date = datetime.strptime(line.split()[-1].strip(), "%m/%d/%Y")
        elif line.startswith("#"):
            row["rank"] = int(line.split()[0].lstrip("#"))
            row["name"] = " ".join(line.split()[1:])
        elif line.startswith("Level"):
            row["level"] = int(line.split()[1])
            row["xp"] = int(line.split()[-1].rstrip("xp"))
            data.append({"date": date, **row})
            row = {}
    return data


def main():
    raw = Path(__file__).parent / "data/exp.txt"
    raw_text = raw.read_text()
    data = parse_raw(raw_text.split("\n"))
    df = pd.DataFrame(data)
    print(df)
    df.to_csv("data/parsed_exp.csv", index=False)


if __name__ == "__main__":
    main()
