import matplotlib.pyplot as plt
import pandas as pd
import click


def plot_name(df, name, metric="cumulative_xp"):
    subset = df[df.name.str.contains(name)].groupby("date").max().reset_index()
    # generate new metrics
    subset["cumulative_xp"] = subset.xp
    subset["xp"] = subset.xp.diff()
    plt.plot(subset.date, subset[metric], label=name)


@click.command()
@click.argument("names", type=str)
@click.option("--metric", type=str, default="cumulative_xp")
def main(names, metric):
    names = names.split(",")

    df = pd.read_csv("data/parsed_exp.csv")
    df["date"] = pd.to_datetime(df.date)

    for name in names:
        plot_name(df, name, metric)

    metric_label = " ".join(metric.split("_"))
    plt.title(f"{metric_label} over time")
    plt.gcf().autofmt_xdate()
    plt.legend()
    plt.xlabel("date")
    plt.ylabel(metric_label)
    plt.show()


if __name__ == "__main__":
    main()
