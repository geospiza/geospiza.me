import re
import shutil
from pathlib import Path

import pandas as pd
import requests
import urllib3
from openpyxl import load_workbook
from tqdm import tqdm

import genanki
import datetime

# bbb is a bit annoying
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def parse_image_cell(cell):
    return cell.split('("')[1].split('",')[0].split('"')[0]


def parse_sheet(df, category):
    rows, cols = df.shape
    res = []
    for i in range(rows // 2):
        res += [
            dict(image=parse_image_cell(image), name=name, category=category)
            for image, name in zip(df.values[i * 2], df.values[i * 2 + 1])
            if image is not None and name is not None
        ]
    return res


def compute_filename(entry):
    filetype = entry["image"].split(".")[-1].split("?")[0]
    category = entry["category"]
    name = entry["name"]
    return (
        "_".join(re.sub("[^0-9a-zA-Z ]+", "", f"{category} {name}".lower()).split())
        + f".{filetype}"
    )


def download_cache(output, metadata):
    # store images using the category and name
    for entry in tqdm(metadata):
        path = output / compute_filename(entry)
        if path.exists():
            continue
        # download image: https://stackoverflow.com/a/18043472
        # ignore ssl stuff: https://stackoverflow.com/a/12864892
        resp = requests.get(entry["image"], stream=True, verify=False)
        with path.open("wb") as fp:
            shutil.copyfileobj(resp.raw, fp)


def main():
    wb = load_workbook(filename="data/speed-quiz-cheatsheet.xlsx")

    metadata = []
    for sheet_name in wb.sheetnames[1:]:
        ws = wb[sheet_name]
        df = pd.DataFrame(ws.values)
        metadata += parse_sheet(df, sheet_name)

    print(f"parsed {len(metadata)} items")

    images = Path("data/raw_images")
    images.mkdir(parents=True, exist_ok=True)
    download_cache(images, metadata)

    center = "<style>div { text-align: center; } img { width: 200px }</style> "
    model = genanki.Model(
        2059075761,
        "ImageNameRecall",
        fields=[{"name": "Image"}, {"name": "Answer"}],
        templates=[
            {
                "name": "Card 1",
                "qfmt": center + "<div>{{Image}}</div>",
                "afmt": center
                + '{{FrontSide}}<hr id="answer"><div><h1>{{Answer}}</h1></div>',
            }
        ],
    )

    source = "https://docs.google.com/spreadsheets/d/1zoFfGplQullv7DbscQNJVd0Tz494r5q5FXQ-8Xq2df0/edit#gid=418204065"
    deck = genanki.Deck(
        1714339804,
        "MapleLegends Speed Quiz",
        (
            f"An Anki deck adapted from the <a href='{source}'>Speed Quiz Cheat Sheet by Mii, Potatoee and thedgafclub (Summer 2020)</a>. "
            f"Compiled by <a href='https://geospiza.me'>geospiza</a> on {datetime.datetime.utcnow().isoformat()[:10]}."
        ),
    )

    # add notes to the deck
    for entry in metadata:
        filename = compute_filename(entry)
        note = genanki.Note(
            model=model, fields=[f'<img src="{filename}">', entry["name"]]
        )
        deck.add_note(note)

    # add media to package
    package = genanki.Package(deck)
    package.media_files = list(images.glob("*"))
    package.write_to_file("data/ml-speed-quiz-deck.apkg")


if __name__ == "__main__":
    main()
