from subprocess import run
from pathlib import Path

path = Path("data/ml-speed-quiz-deck.apkg")
run(
    f"gsutil cp {path} gs://geospiza/downloads/{path.name}".split(),
    check=True,
    shell=True,
)
