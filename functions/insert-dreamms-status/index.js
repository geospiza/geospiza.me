/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

const fetch = require("node-fetch");
const { BigQuery } = require("@google-cloud/bigquery");

// proxy results through a cheap vps
let base_url = "https://api.geospiza.me/api/v1/maple/dream";

exports.insertStats = async (req, res) => {
  let resp = await fetch(base_url);
  let stats = await resp.json();
  const bigquery = new BigQuery();
  const datasetId = "dreamms";
  const tableId = "status";
  await bigquery.dataset(datasetId).table(tableId).insert([stats]);
  res.status(200).send(JSON.stringify(stats));
};
