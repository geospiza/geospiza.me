/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

const fetch = require("node-fetch");
const { BigQuery } = require("@google-cloud/bigquery");

let base_url = "https://maplelegends.com";

async function get_character(name) {
  let character = await fetch(
    base_url + `/api/character?name=${name}`
  ).then((res) => res.json());
  return {
    timestamp: new Date().toISOString(),
    ...character,
  };
}

exports.insertCharacter = async (req, res) => {
  let data = await get_character("geospiza");
  const bigquery = new BigQuery();
  const datasetId = "maplelegends";
  const tableId = "character";
  await bigquery.dataset(datasetId).table(tableId).insert([data]);
  res.status(200).send(JSON.stringify(data));
};
