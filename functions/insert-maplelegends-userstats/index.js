/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

const fetch = require("node-fetch");
const { BigQuery } = require("@google-cloud/bigquery");

let base_url = "https://maplelegends.com";

async function get_user_stats() {
  let online = await fetch(base_url + "/api/get_online_users").then((res) =>
    res.json()
  );
  let unique = await fetch(base_url + "/api/get_unique_users").then((res) =>
    res.json()
  );
  return {
    timestamp: new Date().toISOString(),
    ...online,
    ...unique,
  };
}

exports.insertStats = async (req, res) => {
  let stats = await get_user_stats();
  const bigquery = new BigQuery();
  const datasetId = "maplelegends";
  const tableId = "userstats";
  await bigquery.dataset(datasetId).table(tableId).insert([stats]);
  res.status(200).send(JSON.stringify(stats));
};
