/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */

const fetch = require("node-fetch");
const { BigQuery } = require("@google-cloud/bigquery");

let base_url = "https://mapleroyals.com";

async function get_user_stats() {
  let status = await fetch(base_url + "/api/v1/get_status").then((res) =>
    res.json()
  );
  return {
    timestamp: new Date().toISOString(),
    ...status,
  };
}

exports.insertStats = async (req, res) => {
  let stats = await get_user_stats();
  const bigquery = new BigQuery();
  const datasetId = "mapleroyals";
  const tableId = "status";
  await bigquery.dataset(datasetId).table(tableId).insert([stats]);
  res.status(200).send(JSON.stringify(stats));
};
