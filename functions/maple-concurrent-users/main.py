import io

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
from flask import Response
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure


def canonize(df, field, start_ds, smooth=0):
    start, end = df.timestamp.min(), df.timestamp.max()
    ds_range = pd.date_range(start, end, freq="15T")
    canon_dates = pd.DataFrame(ds_range, columns=["timestamp"])

    df = (
        df.groupby("timestamp")
        .min()
        .reset_index()
        .merge(canon_dates, on="timestamp", how="right")
        .fillna(0)
        .sort_values("timestamp")
        .set_index("timestamp")
    )

    if smooth > 0:
        df["y"] = df[field].rolling(4 * 24 * smooth).mean()
    else:
        df["y"] = df[field]
    return df[df.index > start_ds]


def main(request):
    """
    Parameters:
        days: number
    """
    days = float(request.args.get("days", "3"))
    smooth = int(request.args.get("smooth", "0"))

    mr = pd.read_json(
        "https://storage.googleapis.com/geospiza/query/mapleroyals-online-count.json"
    )
    ml = pd.read_json(
        "https://storage.googleapis.com/geospiza/query/maplelegends-online-count.json"
    )
    dream = pd.read_json(
        "https://storage.googleapis.com/geospiza/query/dreamms-online-count.json"
    )

    mr.timestamp = mr.timestamp.dt.round("15min")
    ml.timestamp = ml.timestamp.dt.round("15min")
    dream.timestamp = dream.timestamp.dt.round("15min")

    ds = (datetime.now() - timedelta(days)).isoformat()

    ml_trunc = canonize(ml, "usercount", ds, smooth)
    mr_trunc = canonize(mr, "onlineCount", ds, smooth)
    dream_trunc = canonize(dream, "usercount", ds, smooth)

    plt.clf()
    plt.title("number of concurrent users online (15m polling)")
    plt.plot(ml_trunc.y, label="maplelegends")
    plt.plot(mr_trunc.y, label="mapleroyals")
    plt.plot(dream_trunc.y, label="dream")
    plt.tick_params(axis="x", rotation=45)
    plt.legend()
    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.2)
    plt.show()

    # https://stackoverflow.com/a/50728936
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype="image/png")


if __name__ == "__main__":

    class Request:
        def __init__(self, args):
            self.args = args

    main(Request(dict(days=28, smooth=7)))
