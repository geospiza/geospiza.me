import requests
from flask import redirect, jsonify


def main(request):
    """Simply redirect the user to the full api, without CORS checks."""
    path = request.full_path.lstrip("/")
    resp = requests.get(f"https://maplelegends.com/api/{path}")

    if "maplelegends.com" in resp.url:
        # https://stackoverflow.com/a/33091782
        # download the as json data
        resp = jsonify(resp.json())
    else:
        resp = redirect(resp.url)

    resp.headers.add("Access-Control-Allow-Origin", "*")
    return resp
