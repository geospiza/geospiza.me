import requests
from flask import redirect, render_template


def main(request):
    name = request.args.get("name")
    if not name:
        return render_template("index.html")

    resp = requests.get(f"https://maplelegends.com/api/getavatar?name={name}")
    start, end = resp.url.split("/stand")
    uid = 1012569
    # instead of using quote, just embed directly
    part = f",%7B%22itemId%22:{uid},%22version%22:%22213%22%7D"
    url = start + part + "/stand" + end
    return redirect(url)


if __name__ == "__main__":

    class Request:
        def __init__(self, args):
            self.args = args

    main(Request(dict(name="geospiza")))
