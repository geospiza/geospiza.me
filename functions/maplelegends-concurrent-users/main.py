import io

from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from flask import Response
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure


def main(request):
    """
    Parameters:
        days: number
    """
    days = float(request.args.get("days", "14"))
    denoise = bool(request.args.get("denoise"))
    smooth = int(request.args.get("smooth", "1"))

    ml = pd.read_json(
        "https://storage.googleapis.com/geospiza/query/maplelegends-online-count.json"
    )

    ml.timestamp = ml.timestamp.dt.round("15min")
    ml = ml[ml.timestamp >= "2020-07"]

    start, end = ml.timestamp.min(), ml.timestamp.max()
    ds_range = pd.date_range(start, end, freq="15T")
    canon_dates = pd.DataFrame(ds_range, columns=["timestamp"])

    df = (
        ml.groupby("timestamp")
        .min()
        .reset_index()
        .merge(canon_dates, on="timestamp", how="right")
        .fillna(0)
        .sort_values("timestamp")
    )

    staged = df[
        df.timestamp
        > (datetime.now() - (timedelta(days) + timedelta(smooth)) * 2).isoformat()
    ].copy()

    if denoise:
        med = staged.usercount.median()
        mad = (staged.usercount - med).abs().median()
        k = 1 / 0.6745
        staged["score"] = (staged.usercount - med) / (k * mad)
        staged["normed"] = staged.usercount
        staged.loc[
            (staged.score.abs() > 3) | (staged.usercount == 0), "normed"
        ] = np.nan
        y = staged.normed.interpolate()
    else:
        y = staged.usercount.interpolate()
    if smooth:
        y = y.rolling(4 * 24 * smooth).mean()

    staged["y"] = y
    trunc = staged[staged.timestamp > (datetime.now() - timedelta(days)).isoformat()]
    prev = staged[
        staged.timestamp < (datetime.now() - timedelta(days)).isoformat()
    ].tail(trunc.shape[0])
    print(trunc)
    print(prev)

    plt.clf()
    plt.title(
        f"concurrent maplelegends online count" + f" ({smooth}-day moving average)"
        if smooth
        else ""
    )
    plt.plot(trunc.timestamp, trunc.y, label=f"maplelegends (last {int(days)} days)")
    plt.plot(trunc.timestamp, prev.y, "--", label=f"maplelegends (previous period)")
    plt.tick_params(axis="x", rotation=45)
    plt.legend()
    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.2)
    plt.show()

    # https://stackoverflow.com/a/50728936
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype="image/png")


if __name__ == "__main__":

    class Request:
        def __init__(self, args):
            self.args = args

    main(Request(dict(days=14, denoise=False, smooth=1)))
