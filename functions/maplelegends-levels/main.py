from flask import jsonify
import requests
from bs4 import BeautifulSoup


def parse_levels_row(tr):
    td = tr.find_all("td")
    return dict(
        level=int(td[0].getText()),
        timestamp=td[1].getText().replace(" ", "T"),
    )


def parse_levels_page(resp):
    soup = BeautifulSoup(resp.text)
    name = resp.url.split("=")[-1]
    rows = soup.find_all("tr")[1:]
    return dict(name=name, history=[parse_levels_row(tr) for tr in rows])


def get_levels(name):
    url = f"https://maplelegends.com/levels?name={name}"
    return requests.get(url)


def main(request):
    name = request.args["name"]
    html = get_levels(name)
    return jsonify(parse_levels_page(html))
