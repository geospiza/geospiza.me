import io
import pickle

from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import requests
from bs4 import BeautifulSoup
from flask import Response, jsonify, render_template
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure


def parse_levels_row(tr):
    td = tr.find_all("td")
    return dict(
        level=int(td[0].getText()),
        timestamp=td[1].getText().replace(" ", "T"),
    )


def parse_levels_page(resp):
    soup = BeautifulSoup(resp.text, features="html.parser")
    name = resp.url.split("=")[-1]
    rows = soup.find_all("tr")[1:]
    return dict(name=name, history=[parse_levels_row(tr) for tr in rows])


def get_levels(name):
    url = f"https://maplelegends.com/levels?name={name}"
    return requests.get(url)


def plot(cph, df, level, name, historical=False, level_df=None):
    s = cph.predict_survival_function(df).join(cph.baseline_survival_)
    x = s.reset_index()
    x.columns = ["time", "char", "baseline"]
    sec_per_hour = 3600
    x["time"] = x.time / sec_per_hour

    ttl = None
    if historical and level_df is not None:
        ttl = level_df[level_df.level == level].ttl.iloc[0] / sec_per_hour

    # get within 10% of the min baseline
    min_base = x.baseline.min() + 0.05
    z = x[x.baseline >= min_base]

    # adjust for ttl, if it's not in frame
    if ttl and z.time.iloc[-1] < ttl:
        print(z.time.iloc[-1])
        # make sure a few more hours are captured
        z = x[x.time <= ttl * 4]
        print(z.time.iloc[-1])

    fig, (ax1, ax2, ax3) = plt.subplots(
        3, 1, figsize=(6, 8), gridspec_kw={"height_ratios": [6, 4, 2]}
    )

    ax1.set_title(
        f"survival function of {name} leveling to {level} {'(historical)' if historical else ''}"
    )
    ax1.plot(
        z.time,
        z.baseline * 100,
        label=f"baseline ({(1-x.baseline.min())*100:.1f}% will level)",
    )
    ax1.plot(
        z.time,
        z.char * 100,
        label=f"character ({(1-x.char.min())*100:.1f}% will level)",
    )
    # get the actual time to level
    if ttl:
        ax1.axvline(
            x=ttl, linestyle="dashed", label=f"actual time to level ({ttl:.2f} hours)"
        )
    ax1.set_xlabel("time in hours")
    ax1.set_ylabel(f"% not level {level}")
    ax1.legend()

    ax2.axis("off")
    data = ax2.table(
        cellText=[
            [
                f"{i}th percentile",
                f"{x.loc[x.baseline <= i/100].iloc[0].time:.3f}",
                f"{x.loc[x.char <= i/100].iloc[0].time:.3f}",
            ]
            for i in [90, 75, 50, 25]
            if i / 100 > max([x.char.min(), x.baseline.min()])
        ]
        or [
            [
                f"{i}th percentile",
                f"{x.loc[x.baseline <= i/100].iloc[0].time:.3f}",
                f"{x.loc[x.char <= i/100].iloc[0].time:.3f}",
            ]
            for i in [99, 95, 90, 75]
            if i / 100 > max([x.char.min(), x.baseline.min()])
        ],
        colLabels=["survival", "baseline (hours)", "character (hours)"],
        loc="center",
    )
    data.scale(1, 1.5)

    ax3.set_title(f"model information")
    ax3.axis("off")
    table = ax3.table(
        cellText=[
            ["model", cph._class_name],
            ["number of observations", cph.durations.size],
            ["number of events observed", cph.event_observed.sum()],
            ["time fit was run", cph._time_fit_was_called],
        ],
        loc="center",
    )
    table.scale(1, 1.5)
    return fig


def main(request):
    name = request.args.get("name")
    level = request.args.get("level")
    if level:
        level = int(level)
    if not name:
        return render_template("index.html")

    html = get_levels(name)
    levels = parse_levels_page(html)

    level_df = pd.DataFrame(levels["history"]).sort_values("level")
    level_df["ts"] = pd.to_datetime(level_df.timestamp)
    level_df["ttl"] = level_df.ts.diff().dt.total_seconds()

    exptable_url = (
        "https://storage.googleapis.com/geospiza/ml-player-growth/exptable.csv"
    )
    exp_table = pd.read_csv(exptable_url)
    exp = exp_table.set_index("level").to_dict()["exp"]

    d = {}
    d["observed"] = 0
    for _, row in level_df.iterrows():
        if np.isnan(row.ttl):
            continue
        d[f"ttl_{row.level}"] = np.log(exp[row.level] / row.ttl)

    # NOTE: very confusing indeed
    current_level = level_df.level.max()
    level = max(min(min(level or current_level + 1, current_level + 1), 200), 5)
    historical = level <= current_level

    url = (
        "https://storage.googleapis.com/"
        f"geospiza/ml-player-growth/survival/cox_v2/level_{level}.pickle"
    )
    resp = requests.get(url)
    cph = pickle.loads(resp.content)

    plt.clf()
    fig = plot(
        cph, pd.DataFrame([d]), level, name, historical=historical, level_df=level_df
    )
    plt.show()

    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype="image/png")


if __name__ == "__main__":

    class Request:
        args = {"name": "geospiza"}

    main(Request())
