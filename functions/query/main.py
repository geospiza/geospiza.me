from google.cloud import bigquery, storage
import os
import json


def default_serializer(obj):
    """JSON serializer for objects that cannot be serialized by default json code

    https://stackoverflow.com/a/22238613
    """
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError(f"Type {type(obj)} cannot be serialized")


def dump_to_gcs(bucket, name, query):
    bq = bigquery.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(bucket)
    print(f"running query for {name}: {query}")
    blob = bucket.blob(f"query/{name}.json")
    output = [dict(row) for row in bq.query(query)]
    blob.upload_from_string(
        json.dumps(output, default=default_serializer), content_type="application/json"
    )


def main(request):
    bucket = os.environ.get("BUCKET", "geospiza")

    dump_to_gcs(
        bucket,
        "maplelegends-online-count",
        """
        select
            cast(timestamp as string) as timestamp,
            usercount
        from maplelegends.userstats
        order by timestamp desc
        """,
    )
    dump_to_gcs(
        bucket,
        "maplelegends-online-count-7-day",
        """
        select
            cast(timestamp as string) as timestamp,
            usercount
        from maplelegends.userstats
        where timestamp > timestamp_sub(current_timestamp(), interval 7 day)
        order by timestamp desc
        """,
    )

    dump_to_gcs(
        bucket,
        "mapleroyals-online-count",
        """
        select
            cast(timestamp as string) as timestamp,
            onlineCount
        from mapleroyals.status
        order by timestamp desc
        """,
    )

    dump_to_gcs(
        bucket,
        "dreamms-online-count",
        """
        select
            cast(timestamp as string) as timestamp,
            usercount
        from dreamms.status
        order by timestamp desc
        """,
    )

    return "OK"


if __name__ == "__main__":
    main(None)
