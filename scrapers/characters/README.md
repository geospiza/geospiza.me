# Ranking spider

```bash
scrapy runspider main.py -o data/data.json
```

To sync the data:

```bash
gsutil rsync -r data/ gs://geospiza/scrapers/characters/data/
```
