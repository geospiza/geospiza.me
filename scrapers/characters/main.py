import scrapy
import random

TOTAL_RESULT = 164510
PER_PAGE = 5


class MapleLegendsRankingSpider(scrapy.Spider):
    name = "mlranking"
    start_urls = [
        f"https://maplelegends.com/ranking/all?page={page+1}"
        for page in random.sample(range(TOTAL_RESULT // PER_PAGE), 5000 // PER_PAGE)
    ]
    dont_redirect = True
    handle_httpstatus_list = [301, 302]
    custom_settings = {
        "DOWNLOAD_DELAY": 0.25,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 2,
        "ITEM_PIPELINES": {"scrapy.pipelines.images.ImagesPipeline": 1},
        "IMAGES_STORE": "data",
        # "DOWNLOADER_MIDDLEWARES": {
        #     "scrapy.downloadermiddlewares.redirect.RedirectMiddleware": 100
        # },
        # "REDIRECT_ENABLED": True,
        "MEDIA_ALLOW_REDIRECTS": True,
    }

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        for sel in response.xpath("//img[contains(@class, 'avatar')]/@src"):
            src = sel.get()
            yield dict(
                image_urls=[f"https://maplelegends.com{src}"], name=src.split("=")[-1],
            )

