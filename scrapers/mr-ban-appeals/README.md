# Ban appeals on the MapleRoyals forums

```bash
scrapy runspider section.py -o data/2021-01-03-section.json
scrapy runspider thread.py -o data/2021-01-03-thread.json
```

To sync the data:

```bash
gsutil rsync -r data/ gs://geospiza/scrapers/mr-ban-appeals/data/
```

## examples

```json
{
  "prefix_href": "forums/8/?prefix_id=18",
  "member_href": "members/krazykace.28798/",
  "username": "krazykace",
  "start_date": null,
  "thread_href": "threads/ban-appeal.181629/",
  "title": "Ban Appeal",
  "replies": 7,
  "views": 913
}
```
