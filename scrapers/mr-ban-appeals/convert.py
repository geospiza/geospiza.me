from pyspark.sql import SparkSession, functions as F
from sqlalchemy import create_engine

spark = SparkSession.builder.getOrCreate()
section = spark.read.json("data/2021-01-03-section.json", multiLine=True)
thread = spark.read.json("data/2021-01-03-thread.json", multiLine=True)
exploded = thread.withColumn("items", F.explode("thread_items")).select(
    "thread_href", "items.*"
)


def get_id(col):
    return F.regexp_extract(col, "(\d+)/", 1).astype("int")


posts = exploded.select(
    get_id("thread_href").alias("thread_id"),
    get_id("member_href").alias("member_id"),
    F.col("post_number").astype("int"),
    # Nov 24, 2020 at 12:54 PM
    # https://docs.oracle.com/javase/tutorial/i18n/format/simpleDateFormat.html
    F.to_timestamp("timestamp", "MMM d, yyyy 'at' h:mm a").alias("timestamp"),
    "text",
)

members = (
    exploded.select(
        get_id("member_href").alias("member_id"),
        F.to_date("member_joined", "MMM d, yyyy").alias("member_joined"),
        "username",
        "member_href",
        "staff",
    )
    .groupBy("member_id", "member_joined", "username", "staff", "member_href")
    .agg(F.count("*").astype("int").alias("posts_in_section"))
    .orderBy("member_id")
)

threads = section.select(
    get_id("thread_href").alias("thread_id"),
    get_id("member_href").alias("member_id"),
    F.to_date("start_date", "MMM d, yyyy").alias("start_date"),
    "thread_href",
    "title",
    F.coalesce(
        F.regexp_extract("prefix_href", "prefix_id=(\d)+", 1).astype("int"), F.lit(0)
    ).alias("prefix_id"),
    F.col("views").astype("int"),
    F.col("replies").astype("int"),
)


engine = create_engine("sqlite:///data/appeals.db", echo=True)
conn = engine.connect()

posts.toPandas().set_index("thread_id").to_sql("posts", conn, if_exists="replace")
members.toPandas().set_index("member_id").to_sql("members", conn, if_exists="replace")
threads.toPandas().set_index("thread_id").to_sql("threads", conn, if_exists="replace")