import scrapy

# as of 2021-01-03
N_PAGES = 719


class MapleRoyalsBanAppealSection(scrapy.Spider):
    name = "ban-appeal-section"
    # skip the first page because it's strange, and also too recent
    start_urls = [
        f"https://mapleroyals.com/forum/forums/ban-appeal.8/page-{page+1}"
        for page in range(1, N_PAGES)
    ]
    # handle_httpstatus_list = [301, 302]
    custom_settings = {
        "DOWNLOAD_DELAY": 0.25,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 2,
    }

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        for row in response.xpath("//ol[contains(@class,'discussionListItems')]//li"):
            yield dict(
                #  'forums/8/?prefix_id=17'
                prefix_href=row.xpath(
                    ".//a[contains(@class, 'prefixLink')]/@href"
                ).get(),
                member_href=row.xpath(".//a[contains(@class, 'username')]/@href").get(),
                username=row.xpath(".//a[contains(@class, 'username')]/text()").get(),
                # 'Dec 22, 2020', can be parsed out later
                start_date=row.xpath(
                    ".//span[contains(@class, 'startDate')]//span/text()"
                ).get(),
                thread_href=row.xpath(
                    ".//a[contains(@class, 'PreviewTooltip')]/@href"
                ).get(),
                title=row.xpath(
                    ".//a[contains(@class, 'PreviewTooltip')]/text()"
                ).get(),
                replies=int(
                    (
                        row.xpath(".//dl[contains(@class, 'major')]/dd/text()").get()
                        or "0"
                    ).replace(",", "")
                ),
                views=int(
                    (
                        row.xpath(".//dl[contains(@class, 'minor')]/dd/text()").get()
                        or "0"
                    ).replace(",", "")
                ),
            )

