import scrapy
import json
from pathlib import Path

data = json.loads(Path("data/2021-01-03-section.json").read_text())
threads = [d["thread_href"] for d in data]


class MapleRoyalsBanAppealSection(scrapy.Spider):
    name = "ban-appeal-thread"
    start_urls = [f"https://mapleroyals.com/forum/{thread}" for thread in threads]
    custom_settings = {
        "DOWNLOAD_DELAY": 0.5,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 2,
    }

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        results = []
        for i, row in enumerate(response.xpath("//li[contains(@class, 'message')]")):
            # I'll be yielding a row per thread, and then do some
            # processing in pyspark to get this into a nice format.
            d = dict(
                staff="staff" in row.xpath("@class").get(),
                username=row.xpath("@data-author").get(),
                member_href=row.xpath(".//a[contains(@class, 'username')]/@href").get(),
                member_joined=row.xpath(
                    ".//dl[contains(@class, 'pairsJustified')]/dd/text()"
                ).get(),
                text="\n".join(
                    [
                        t.get().strip()
                        for t in row.xpath(
                            # damn it, I have to scrape everything again...
                            ".//blockquote[contains(@class, 'messageText')]//text()"
                        )
                    ]
                ).strip(),
                post_number=i + 1,
                timestamp=row.xpath(
                    ".//a[contains(@class, 'datePermalink')]/span/@title"
                ).get(),
            )
            results.append(d)
        yield dict(thread_href=response.url.split("forum/")[-1], thread_items=results)
