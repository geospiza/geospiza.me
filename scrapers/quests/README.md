# scraping quests

```bash
scrapy runspider mllib.py -o data/mllib.json
scrapy runspider bbb_hiddenstreet.py -o data/bbb_hiddenstreet.json
scrapy runspider gms_hiddenstreet.py -o data/gms_hiddenstreet.json
scrapy runspider sea_hiddenstreet.py -o data/sea_hiddenstreet.json
scrapy runspider manual_hiddenstreet.py -o data/manual_hiddenstreet.json
# set OPTION=1
scrapy runspider manual_hiddenstreet.py -o data/manual_hiddenstreet_v1.json
scrapy runspider manual_hiddenstreet.py -o data/manual_hiddenstreet_v2.json
scrapy runspider manual_hiddenstreet.py -o data/manual_hiddenstreet_v3.json
```

Data is stored in GCS, in case this needs to be revisited.

```bash
gsutil rsync -r data/ gs://geospiza/scrapers/quest/data/
```

This is some of the ugliest code I've written over the period of a week. It
started off as a way to explore how quests are structured in MapleStory. How
many quests are available for each level, and is it possible to structure it
in such a way where there's some sort of quest leader-board?

The scraped data is ugly. It requires scraping from multiple hidden-street
sources, including the old before-big-bang site and the current up to date
GMS/EMS/SEA site. The first script, `bbb_hiddenstreet.py` started scraping from
a listing page and went into each element. The second took the list of values
take from bbb and searched for them on the hiddenstreet site using the built in
search functionality. The same was done for the sea site. Finally, there were
several iterations of manually finding the pages using Google and a lot of
copying and pasting into starter lists. In hindsight, it may be easier just to
scrape from the quests related to specific characters, or the whole site.

Finally, this thing can't be totally automated because the data is dirty. I've
gotten 80% there with automated methods, but I'll have to go in and clean it up
by hand.
