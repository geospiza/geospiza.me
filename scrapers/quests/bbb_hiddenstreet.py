import scrapy


class BBBHiddenStreet(scrapy.Spider):
    name = "bbb-hiddenstreet"
    # there are actually 49 pages
    start_urls = ["https://bbb.hidden-street.net/quest/list"]
    custom_settings = {"DOWNLOAD_DELAY": 0.25, "CONCURRENT_REQUESTS_PER_DOMAIN": 2}

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        for row in response.xpath("//tr")[1:]:
            level = row.xpath(".//td[contains(@class, 'active')]/text()").get().strip()
            link = row.xpath(".//a/@href").get()
            yield scrapy.Request(
                response.urljoin(link), callback=self.parse_item, meta=dict(level=level)
            )

    def parse_item(self, response):
        for table in response.xpath("//table"):
            yield dict(
                level=response.meta["level"],
                href=response.url,
                name=table.xpath("tr[1]//strong/text()").get().strip(),
                quest_type=table.xpath("tr[1]//div")[1]
                .xpath(".//text()")
                .get()
                .strip(),
                meso=table.xpath("tr")[-1]
                .xpath(".//li[contains(., 'mesos')]/text()")
                .get(),
                experience=table.xpath("tr")[-1]
                .xpath(".//li[contains(., 'experience')]/text()")
                .get(),
            )

