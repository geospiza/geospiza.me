import scrapy
from pathlib import Path

OPTION = 3
paths = [
    "data/mllib_disjoint_manual.txt",
    "data/mllib_corrections.txt",
    "data/mllib_corrections_v2.txt",
    "data/mllib_corrections_v3.txt",
]


class ManualHiddenStreet(scrapy.Spider):
    name = "manual-hiddenstreet"
    start_urls = [
        url
        for url in Path(paths[OPTION]).read_text().split("\n")
        if not url.startswith("#")
    ]
    custom_settings = {"DOWNLOAD_DELAY": 1, "CONCURRENT_REQUESTS_PER_DOMAIN": 2}

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        for table in response.xpath("//table"):
            yield dict(
                level=table.xpath("tr[1]//div[contains(., 'Level')]//text()").get(),
                href=response.url,
                name=table.xpath("tr[1]//strong/text()").get().strip(),
                quest_type=response.xpath(
                    "//div[contains(@class, 'desc-info')]//div[contains(@class, 'field-items')]//text()"
                ).get(),
                meso=table.xpath("tr")[-1]
                .xpath(".//li[contains(., 'mesos')]/text()")
                .get(),
                experience=table.xpath("tr")[-1]
                .xpath(".//li[contains(., 'experience')]/text()")
                .get(),
            )

