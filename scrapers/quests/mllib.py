import scrapy


class MapleLegendsLibrarySpider(scrapy.Spider):
    name = "mllibspider"
    # there are actually 161 pages as of 2020-10-06
    start_urls = [
        f"https://maplelegends.com/lib/quest?page={page+1}" for page in range(161)
    ]
    custom_settings = {"DOWNLOAD_DELAY": 0.25, "CONCURRENT_REQUESTS_PER_DOMAIN": 2}

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        for sel in response.xpath("//tr")[1:]:
            yield dict(
                quest=sel.xpath("td[3]/a/text()").get(),
                quest_href=sel.xpath("td[3]/a/@href").get(),
                npc=sel.xpath("td[2]/a/text()").get(),
                npc_href=sel.xpath("td[2]/a/@href").get(),
            )
