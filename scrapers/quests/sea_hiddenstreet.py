import scrapy
from pathlib import Path

disjoint_bbb = Path("data/mllib_disjoint_bbb_gms.txt").read_text().split("\n")


class SEAHiddenStreet(scrapy.Spider):
    name = "sea-hiddenstreet"
    start_urls = [
        # not, not using urlencode and letting scrapy deal with it
        f"https://www.hidden-street.net/sea/search?key={value}"
        for value in disjoint_bbb
    ]
    custom_settings = {"DOWNLOAD_DELAY": 1, "CONCURRENT_REQUESTS_PER_DOMAIN": 2}

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        quest = response.xpath("//article[contains(@class, 'node-quest')]")
        if not quest:
            return
        link = quest.xpath(".//a/@href").get()
        yield scrapy.Request(response.urljoin(link), callback=self.parse_item)

    def parse_item(self, response):
        for table in response.xpath("//table"):
            yield dict(
                level=table.xpath("tr[1]//div[contains(., 'Level')]//text()").get(),
                href=response.url,
                name=table.xpath("tr[1]//strong/text()").get().strip(),
                quest_type=response.xpath(
                    "//div[contains(@class, 'desc-info')]//div[contains(@class, 'field-items')]//text()"
                ).get(),
                meso=table.xpath("tr")[-1]
                .xpath(".//li[contains(., 'mesos')]/text()")
                .get(),
                experience=table.xpath("tr")[-1]
                .xpath(".//li[contains(., 'experience')]/text()")
                .get(),
            )

