# Scraping level 200 characters on maplelegends

```bash
scrapy runspider ranking.py -o data/ranking.json
scrapy runspider leveling.py -o data/leveling.json
```

Create the SQLite database

```bash
python convert.py
```

To sync the data:

```bash
gsutil rsync -r data/ gs://geospiza/scrapers/ranking/data/
```
