import pandas as pd
from sqlalchemy import create_engine

ranking = pd.read_json("data/ranking.json").sort_values(["rank"])
leveling = pd.read_json("data/leveling.json").sort_values(["name", "level"])

engine = create_engine("sqlite:///data/ranking.db", echo=True)
conn = engine.connect()

ranking.set_index("name").to_sql("ranking", conn, if_exists="replace")
leveling.set_index("name").to_sql("leveling", conn, if_exists="replace")
