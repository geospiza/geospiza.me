"""Get the leveling history."""
from pathlib import Path
import json
import scrapy


path = Path("data/ranking.json")
if not path.exists():
    raise RuntimeError(f"{path} does not exist")
data = json.loads(path.read_text())


class MapleLegendsLevelingHistorySpider(scrapy.Spider):
    name = "leveling_history"
    start_urls = [f"https://maplelegends.com/levels?name={row['name']}" for row in data]
    dont_redirect = True
    handle_httpstatus_list = [301, 302]
    custom_settings = {
        "DOWNLOAD_DELAY": 0.25,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 2,
    }

    def parse(self, response):
        for row in response.xpath("//tr")[1:]:
            yield dict(
                name=response.url.split("=")[-1],
                level=int(row.xpath("td[1]/text()").get()),
                timestamp=row.xpath("td[2]/text()").get(),
            )

