import scrapy

N_PAGES = 30


class MapleLegendsRankingSpider(scrapy.Spider):
    name = "ranking"
    start_urls = [
        f"https://maplelegends.com/ranking/all?page={page+1}" for page in range(N_PAGES)
    ]
    dont_redirect = True
    handle_httpstatus_list = [301, 302]
    custom_settings = {
        "DOWNLOAD_DELAY": 0.25,
        "CONCURRENT_REQUESTS_PER_DOMAIN": 2,
    }

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        for row in response.xpath("//tr")[1:]:
            level = int(row.xpath("td[6]/b/text()").get())
            if level < 200:
                continue
            yield dict(
                rank=int(row.xpath("td[1]/b/text()").get()),
                name=row.xpath("td[3]//a/text()").get(),
                guild=row.xpath("td[3]/a/text()").get().strip(),
                job=row.xpath("td[4]/a/text()").get().strip(),
                fame=int(row.xpath("td[5]/b/text()").get()),
            )

