import scrapy


class MapleLegendsLibrarySpider(scrapy.Spider):
    name = "mllibspider"
    # there are actually 49 pages
    start_urls = [
        f"https://maplelegends.com/lib/use?page={page}&search=scroll%20for"
        for page in range(50)
    ]
    custom_settings = {"DOWNLOAD_DELAY": 0.25, "CONCURRENT_REQUESTS_PER_DOMAIN": 2}

    # useful link: https://stackoverflow.com/a/43497095
    def parse(self, response):
        links = response.xpath("//table//a/@href")
        if not links:
            return
        for link in links:
            yield scrapy.Request(response.urljoin(link.get()), callback=self.parse_item)

    def parse_item(self, response):
        data = dict(
            href=response.url,
            name=response.xpath("//table//tr[1]//th/div[1]/text()").get(),
            untradable=response.xpath("//table//tr[1]//th/div[2]/text()").get(),
            desc="\n".join(
                path.get() for path in response.xpath("//table//tr[3]//td/text()")
            ).strip(),
        )
        monsters = []
        for item in response.css(".panel-body").xpath(".//a"):
            monsters.append(
                dict(href=item.xpath("@href").get(), text=item.xpath("text()").get())
            )
        data["monsters"] = monsters
        yield data

