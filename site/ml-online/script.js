function transform(data) {
  return [
    {
      x: data.map((obj) => obj.timestamp.slice(0, -3)),
      y: data.map((obj) => obj.usercount),
      type: "scatter",
      mode: "lines",
    },
  ];
}

async function main() {
  let resp = await fetch(
    "https://storage.googleapis.com/geospiza/query/maplelegends-online-count.json"
  ).catch((err) => {
    let element = document.getElementById("plot");
    element.innerHTML = "<p>Failed to fetch resource.</p>";
    throw err;
  });

  let data = await resp.json();
  data.reverse();

  let endDate = new Date(data[data.length - 1].timestamp.substring(0, 10));
  let startDate = new Date(endDate);
  startDate = startDate.setDate(endDate.getDate() - 3);
  endDate.setDate(endDate.getDate() + 1);
  var layout = {
    title: "[MapleLegends] Online Users over Time",
    xaxis: {
      range: [startDate, endDate],
      rangeselector: {
        buttons: [
          {
            count: 1,
            label: "1d",
            step: "day",
            stepmode: "backward",
          },
          {
            count: 3,
            label: "3d",
            step: "day",
            stepmode: "backward",
          },
          {
            count: 7,
            label: "1w",
            step: "day",
            stepmode: "backward",
          },
          {
            count: 1,
            label: "1m",
            step: "month",
            stepmode: "backward",
          },
          { step: "all" },
        ],
      },
      rangeslider: {
        range: [data[0].timestamp.substring(0, 10), endDate],
      },
      type: "date",
    },
    yaxis: {
      autorange: true,
      type: "linear",
    },
  };

  Plotly.newPlot("plot", transform(data), layout, { responsive: true });
}

main();
