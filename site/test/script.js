async function main() {
  let element = document.getElementById("data");

  let resp = await fetch(
    "https://storage.googleapis.com/geospiza/test.json"
  ).catch((err) => {
    element.innerHTML = "<p>Failed to fetch resource.</p>";
    throw err;
  });

  let data = await resp.json();
  console.log(data);
  element.innerHTML = `<pre>${JSON.stringify(data)}</pre>`;
}

main();
