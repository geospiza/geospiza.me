variable "region" {
  type    = string
  default = "us-west2"
}

variable "zone" {
  type    = string
  default = "us-west2-c"
}

variable "project_id" {
  type    = string
  default = "geospiza"
}

terraform {
  backend "gcs" {
    bucket = "geospiza-terraform"
    prefix = "geospiza-prod"
  }
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

# Apparently this is necessary for cloud scheduler. But why?
resource "google_app_engine_application" "app" {
  project     = var.project_id
  location_id = var.region
}

resource "google_service_account" "app_engine" {
  account_id   = "geospiza"
  display_name = "App Engine default service account"
}


resource "google_storage_bucket" "static_assets" {
  name                        = var.project_id
  location                    = "US"
  uniform_bucket_level_access = true
  cors {
    origin          = ["https://geospiza.me", "https://geospiza.netlify.app"]
    method          = ["*"]
    response_header = ["*"]
    max_age_seconds = 3600
  }
}

resource "google_storage_bucket_iam_binding" "public_rule" {
  bucket = google_storage_bucket.static_assets.name
  role   = "roles/storage.objectViewer"
  members = [
    "allUsers"
  ]
}

resource "google_storage_bucket" "argon_resources" {
  name     = "argon-resources"
  location = "US"
}

resource "google_bigquery_dataset" "maplelegends" {
  dataset_id = "maplelegends"
  location   = "US"
}

resource "google_bigquery_dataset_iam_binding" "maplelegends_public" {
  dataset_id = google_bigquery_dataset.maplelegends.dataset_id
  role       = "roles/bigquery.dataViewer"
  members    = ["allUsers"]
}

resource "google_bigquery_table" "userstats" {
  dataset_id = google_bigquery_dataset.maplelegends.dataset_id
  table_id   = "userstats"
  time_partitioning {
    type = "DAY"
  }
  schema = file("../bigquery/maplelegends.userstats.bq.json")
}

resource "google_bigquery_table" "character" {
  dataset_id = google_bigquery_dataset.maplelegends.dataset_id
  table_id   = "character"
  schema     = file("../bigquery/maplelegends.character.bq.json")
}

// keep some uptime stats on mapleroyals too, just cause
resource "google_bigquery_dataset" "mapleroyals" {
  dataset_id = "mapleroyals"
  location   = "US"
}

resource "google_bigquery_table" "status" {
  dataset_id = google_bigquery_dataset.mapleroyals.dataset_id
  table_id   = "status"
  time_partitioning {
    type = "DAY"
  }
  schema = file("../bigquery/mapleroyals.status.bq.json")
}

resource "google_bigquery_dataset" "dreamms" {
  dataset_id = "dreamms"
  location   = "US"
}

resource "google_bigquery_table" "dreamms_status" {
  dataset_id = google_bigquery_dataset.dreamms.dataset_id
  table_id   = "status"
  time_partitioning {
    type = "DAY"
  }
  schema = file("../bigquery/dreamms.status.bq.json")
}

module "function_scrape_online_counts" {
  for_each = toset([
    "insert-maplelegends-userstats",
    "insert-mapleroyals-status",
    "insert-dreamms-status"
  ])
  source                = "./functions"
  runtime               = "nodejs12"
  name                  = each.value
  entry_point           = "insertStats"
  service_account_email = google_service_account.app_engine.email
  bucket                = google_storage_bucket.static_assets.name
  schedule              = "*/15 * * * *"
  available_memory_mb   = 128
  app_engine_region     = var.region
}


module "function_insert_maplelegends_character" {
  source                = "./functions"
  runtime               = "nodejs12"
  name                  = "insert-maplelegends-character"
  entry_point           = "insertCharacter"
  service_account_email = google_service_account.app_engine.email
  bucket                = google_storage_bucket.static_assets.name
  schedule              = "0 0 */1 * *"
  available_memory_mb   = 128
  app_engine_region     = var.region
}

module "function_query" {
  source                = "./functions"
  name                  = "query"
  entry_point           = "main"
  service_account_email = google_service_account.app_engine.email
  bucket                = google_storage_bucket.static_assets.name
  schedule              = "0 */1 * * *"
  available_memory_mb   = 512
  app_engine_region     = var.region
}

module "functions_python_public" {
  for_each = {
    for entry in [
      { name = "maple-concurrent-users", memory = 512 },
      { name = "maplelegends-concurrent-users", memory = 512 },
      { name = "maplelegends-survival", memory = 512 },
      { name = "maplelegends-levels", memory = 128 },
      { name = "maplelegends-avatar-pacifier", memory = 128 },
      { name = "maplelegends-api-proxy", memory = 128 },
    ] : entry.name => entry
  }
  source                = "./functions"
  name                  = each.value.name
  available_memory_mb   = each.value.memory
  entry_point           = "main"
  service_account_email = google_service_account.app_engine.email
  bucket                = google_storage_bucket.static_assets.name
  schedule              = null
  app_engine_region     = var.region
  public                = true
}

module "ml-exp-tracker" {
  source = "../apps/ml-exp-tracker/terraform"
  bucket = google_storage_bucket.static_assets.name
}
