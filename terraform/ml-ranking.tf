resource "google_bigquery_dataset" "mlrank" {
  dataset_id    = "mlrank"
  friendly_name = "MapleLegends ranking data"
  description   = "MapleLegends ranking data"
  location      = "US"
}

resource "google_bigquery_dataset_iam_binding" "mlrank_public" {
  dataset_id = google_bigquery_dataset.mlrank.dataset_id
  role       = "roles/bigquery.dataViewer"
  members    = ["allUsers"]
}

resource "google_bigquery_table" "exptable" {
  dataset_id = google_bigquery_dataset.mlrank.dataset_id
  table_id   = "exptable"
}

resource "google_bigquery_table" "ranking" {
  dataset_id = google_bigquery_dataset.mlrank.dataset_id
  table_id   = "ranking"
}

resource "google_bigquery_table" "levels" {
  dataset_id = google_bigquery_dataset.mlrank.dataset_id
  table_id   = "levels"
}

// Note that the load jobs can't be changed once they've been created

resource "google_bigquery_job" "exptable" {
  job_id = "exptable-load"

  load {
    source_uris = [
      "gs://geospiza/ml-player-growth/exptable.csv"
    ]

    destination_table {
      project_id = google_bigquery_table.exptable.project
      dataset_id = google_bigquery_table.exptable.dataset_id
      table_id   = google_bigquery_table.exptable.table_id
    }

    skip_leading_rows = 1
    write_disposition = "WRITE_TRUNCATE"
    autodetect        = true
  }
}

resource "google_bigquery_job" "ranking" {
  job_id = "ranking-load"

  load {
    source_uris = [
      "gs://geospiza/ml-player-growth/20210801_all_ranking.csv"
    ]

    destination_table {
      project_id = google_bigquery_table.ranking.project
      dataset_id = google_bigquery_table.ranking.dataset_id
      table_id   = google_bigquery_table.ranking.table_id
    }

    skip_leading_rows = 1
    write_disposition = "WRITE_TRUNCATE"
    autodetect        = true
  }
}

resource "google_bigquery_job" "levels" {
  job_id = "levels-load"

  load {
    source_uris = [
      "gs://geospiza/ml-player-growth/20210801_all_levels.csv"
    ]

    destination_table {
      project_id = google_bigquery_table.levels.project
      dataset_id = google_bigquery_table.levels.dataset_id
      table_id   = google_bigquery_table.levels.table_id
    }

    skip_leading_rows = 1
    write_disposition = "WRITE_TRUNCATE"
    autodetect        = true
  }
}
